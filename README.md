Upute za pokretanje:
Inicijalizirati bazu datotekama danim u DZ_02.
Ako je potrebno, podesiti ConnectionString (user, pass i sl.) na bazu u appsettings.json.

Potrebno je imati instaliran ASP.NET Core 6.

Instalirati pakete u direktoriju projekta SebiMvc:
dotnet add package Microsoft.EntityFrameworkCore
dotnet add package Microsoft.EntityFrameworkCore.Design
dotnet add package Npgsql.EntityFrameworkCore.PostgreSQL
dotnet add package Microsoft.EntityFrameworkCore.SqlServer

U direktoriju projekta SebiMVC izvršiti naredbu za pokretanje aplikacije:
dotnet run Program.cs

Kopirati URL iz ispisa oblika "Now listening on: https://localhost:port" u web preglednik.
Posjetiti URL na kojem je aplikacija.

