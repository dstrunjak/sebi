using System.ComponentModel.DataAnnotations;

namespace DomainModel;

public class StavkaNalog
{
    [Display(Name = "Šifra")]
    public int Idstavka { get; set; }

    [Display(Name = "Radni nalog")]
    public int Idradninalog { get; set; }

    [Display(Name = " Količina artikla")]
    public int? Kolicinaartikl { get; set; }

    [Display(Name = "Količina rada")]
    public int Kolicinarad { get; set; }

    [Display(Name = "Usluga")]
    public int Idrad { get; set; }

    [Display(Name = "Usluga")]
    public string Nazrad { get; set; } = null!;

    [Display(Name = "Status")]
    public int Idstatus { get; set; }

    [Display(Name = "Status")]
    public string Nazstatus { get; set; } = null!;

    [Display(Name = " Artikl")]
    public int? Idartiklservis { get; set; }

    [Display(Name = " Artikl")]
    public string NazArtikl { get; set; } = null!;



}