﻿namespace DomainModel;

public class Osoba
{
    public int Idosoba { get; set; }
    public string Ime { get; set; } = null!;
    public string Prezime { get; set; } = null!;
    public string PunoIme => $"{Ime} {Prezime}";
    public string Email { get; set; } = null!;
    public string Telefon { get; set; } = null!;
    public string Adresa { get; set; } = null!;
    public int Idmjesto { get; set; }
    public List<Bicikl> Bicikli { get; set; } = new List<Bicikl>();
}
