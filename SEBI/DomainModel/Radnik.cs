﻿namespace DomainModel;

public class Radnik: Osoba
{
    public int Idradnik { get; set; }
    public int Voditelj { get; set; }
    public DateOnly Zaposlenod { get; set; }
    public DateOnly? Zaposlendo { get; set; }
    public string? Lozinka { get; set; }
    public int Idservis { get; set; }
}

