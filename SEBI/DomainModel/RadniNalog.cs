using System.ComponentModel.DataAnnotations;

namespace DomainModel;

public class RadniNalog
{
        [Display(Name = "Šifra")]
        public int Idradninalog { get; set; }

        [Display(Name = "Vrijeme izrade", Prompt="Vrijeme izrade")]
        public DateTime Vrizrada { get; set; }

        [Display(Name = "Vrijeme početka", Prompt="Vrijeme početka")]
        public DateTime? Vrpocetak { get; set; }

        [Display(Name = "Vrijeme završetka", Prompt="Vrijeme završetka")]
        public DateTime? Vrzavrsetak { get; set; }

        [Display(Name = "Servis", Prompt="Servis")]
        public int Idservis { get; set; }

        [Display(Name = "Servis")]
        public string NazServis { get; set; } = null!;

        [Display(Name = "Korisnik", Prompt="Korisnik")]
        public int Idosoba { get; set; }

        [Display(Name = "Korisnik")]
        public string PunoImeKorisnik { get; set; } = null!;

        [Display(Name = "Izradio", Prompt="Izradio")]
        public int Izradio { get; set; }

        [Display(Name = "Izradio")]
        public string PunoImeIzradio { get; set; } = null!;

        [Display(Name = "Bicikl", Prompt="Bicikl")]
        public int Idbicikl { get; set; }
        
        [Display(Name = "Bicikl", Prompt="Bicikl")]
        public string Nazbicikl { get; set; } = null!;

        [Display(Name = "Status", Prompt="Status")]
        public int Idstatus { get; set; }

        [Display(Name = "Status")]
        public string Nazstatus { get; set; } = null!;

        [Display(Name = "Obavio", Prompt="Obavio")]
        public int? Obavio { get; set; }

        [Display(Name = "Obavio")]
        public string PunoImeObavio { get; set; } = null!;
        public List<StavkaNalog> Stavke { get; set; } = new List<StavkaNalog>();
}