﻿namespace DomainModel;

public class Bicikl
{
    public int Idbicikl { get; set; }
    public string Nazbicikl { get; set; } = null!;
    public string Serbrbicikl { get; set; } = null!;
    public int Idproizvodac { get; set; }
    public string PunoImeVlasnik { get; set; } = null!;
    
}

