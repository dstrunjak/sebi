using System.ComponentModel.DataAnnotations;
namespace DomainModel;

public class Rad 
{
        [Display(Name = "Šifra")]
        public int Idrad { get; set; }

        [Display(Name = "Naziv", Prompt="Naziv")]
        [Required(ErrorMessage = "Naziv usluge je obavezno polje")]
        public string Nazrad { get; set; } = null!;
       
        [Display(Name = "Cijena / kn", Prompt = "Cijena /kn")]
        [Required(ErrorMessage = "Cijena usluge je obavezno polje")]
        public double Cijena { get; set; }
}