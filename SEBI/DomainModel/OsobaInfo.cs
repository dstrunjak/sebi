namespace DomainModel;

public record OsobaInfo(int Idosoba, string Ime, string Prezime)
{
  public string PunoIme => $"{Ime} {Prezime}";
}