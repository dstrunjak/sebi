﻿namespace DomainModel;

public class Servis
{
    public int Idservis { get; set; }
    public string Nazservis { get; set; } = null!;
    public string Adresaservis { get; set; } = null!;
    public int Vlasnikservis { get; set; }
    public int Idmjesto { get; set; }

}

