dotnet tool uninstall --global dotnet-aspnet-codegenerator
dotnet tool install --global dotnet-aspnet-codegenerator
dotnet tool uninstall --global dotnet-ef
dotnet tool install --global dotnet-ef
dotnet add package Microsoft.EntityFrameworkCore
dotnet add package Microsoft.EntityFrameworkCore.Design
dotnet add package Npgsql.EntityFrameworkCore.PostgreSQL
dotnet add package Microsoft.EntityFrameworkCore.SqlServer
dotnet add package Microsoft.VisualStudio.Web.CodeGeneration.Design
dotnet add package AutoMapper.Extensions.Microsoft.DependencyInjection

! ako se koristi generirani mvc projekt potrebno je maknuti Models direktorij i sva referenciranja namespacea zato što se generira model errora !

dotnet-ef dbcontext scaffold "Username=postgres;Password=bazepodataka;Host=localhost;Database=SEBI" Npgsql.EntityFrameworkCore.PostgreSQL -o Models


Generiranje:

dotnet-aspnet-codegenerator controller -name RadniNalogController -m Radninalog -dc SEBIContext --relativeFolderPath Controllers --useDefaultLayout --referenceScriptLibraries 

dotnet-aspnet-codegenerator controller -name RadController -m Rad -dc SEBIContext --relativeFolderPath Controllers --useDefaultLayout --referenceScriptLibraries

