﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
// using Npgsql.EntityFrameworkCore.PostgreSQL;

namespace Infrastructure.EFModels
{
    public partial class SEBIContext : DbContext
    {
        // public SEBIContext()
        // {
        // }

        public SEBIContext(DbContextOptions<SEBIContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Artikl> Artikls { get; set; } = null!;
        public virtual DbSet<Artiklservi> Artiklservis { get; set; } = null!;
        public virtual DbSet<Bicikl> Bicikls { get; set; } = null!;
        public virtual DbSet<Mjesto> Mjestos { get; set; } = null!;
        public virtual DbSet<Osoba> Osobas { get; set; } = null!;
        public virtual DbSet<Proizvodac> Proizvodacs { get; set; } = null!;
        public virtual DbSet<Rad> Rads { get; set; } = null!;
        public virtual DbSet<Radnik> Radniks { get; set; } = null!;
        public virtual DbSet<Radninalog> Radninalogs { get; set; } = null!;
        public virtual DbSet<Servi> Servis { get; set; } = null!;
        public virtual DbSet<Skladiste> Skladistes { get; set; } = null!;
        public virtual DbSet<Statusposla> Statusposlas { get; set; } = null!;
        public virtual DbSet<Stavkanalog> Stavkanalogs { get; set; } = null!;

//         protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
//         {
//             if (!optionsBuilder.IsConfigured)
//             {
// // #warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
//                 optionsBuilder.UseNpgsql("Username=postgres;Password=bazepodataka;Host=localhost;Database=SEBI");
//             }
//         }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Artikl>(entity =>
            {
                entity.HasKey(e => e.Idartikl)
                    .HasName("artikl_pkey");

                entity.ToTable("artikl");

                entity.Property(e => e.Idartikl).HasColumnName("idartikl");

                entity.Property(e => e.Idproizvodac).HasColumnName("idproizvodac");

                entity.Property(e => e.Nazartikl)
                    .HasMaxLength(50)
                    .HasColumnName("nazartikl");

                entity.Property(e => e.Serbrartikl)
                    .HasMaxLength(50)
                    .HasColumnName("serbrartikl");

                entity.HasOne(d => d.IdproizvodacNavigation)
                    .WithMany(p => p.Artikls)
                    .HasForeignKey(d => d.Idproizvodac)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("artikl_idproizvodac_fkey");
            });

            modelBuilder.Entity<Artiklservi>(entity =>
            {
                entity.HasKey(e => e.Idartiklservis)
                    .HasName("artiklservis_pkey");

                entity.ToTable("artiklservis");

                entity.Property(e => e.Idartiklservis).HasColumnName("idartiklservis");

                entity.Property(e => e.Cijenakom).HasColumnName("cijenakom");

                entity.Property(e => e.Idartikl).HasColumnName("idartikl");

                entity.Property(e => e.Idservis).HasColumnName("idservis");

                entity.Property(e => e.Idskladiste).HasColumnName("idskladiste");

                entity.Property(e => e.Kolicina).HasColumnName("kolicina");

                entity.HasOne(d => d.IdartiklNavigation)
                    .WithMany(p => p.Artiklservis)
                    .HasForeignKey(d => d.Idartikl)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("artiklservis_idartikl_fkey");

                entity.HasOne(d => d.IdservisNavigation)
                    .WithMany(p => p.Artiklservis)
                    .HasForeignKey(d => d.Idservis)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("artiklservis_idservis_fkey");

                entity.HasOne(d => d.IdskladisteNavigation)
                    .WithMany(p => p.Artiklservis)
                    .HasForeignKey(d => d.Idskladiste)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("artiklservis_idskladiste_fkey");
            });

            modelBuilder.Entity<Bicikl>(entity =>
            {
                entity.HasKey(e => e.Idbicikl)
                    .HasName("bicikl_pkey");

                entity.ToTable("bicikl");

                entity.Property(e => e.Idbicikl).HasColumnName("idbicikl");

                entity.Property(e => e.Idproizvodac).HasColumnName("idproizvodac");

                entity.Property(e => e.Nazbicikl)
                    .HasMaxLength(50)
                    .HasColumnName("nazbicikl");

                entity.Property(e => e.Serbrbicikl)
                    .HasMaxLength(50)
                    .HasColumnName("serbrbicikl");

                entity.Property(e => e.Vlasnikbicikl).HasColumnName("vlasnikbicikl");

                entity.HasOne(d => d.IdproizvodacNavigation)
                    .WithMany(p => p.Bicikls)
                    .HasForeignKey(d => d.Idproizvodac)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("bicikl_idproizvodac_fkey");

                entity.HasOne(d => d.VlasnikbiciklNavigation)
                    .WithMany(p => p.Bicikls)
                    .HasForeignKey(d => d.Vlasnikbicikl)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("bicikl_vlasnikbicikl_fkey");
            });

            modelBuilder.Entity<Mjesto>(entity =>
            {
                entity.HasKey(e => e.Idmjesto)
                    .HasName("mjesto_pkey");

                entity.ToTable("mjesto");

                entity.Property(e => e.Idmjesto).HasColumnName("idmjesto");

                entity.Property(e => e.Nazmjesto)
                    .HasMaxLength(50)
                    .HasColumnName("nazmjesto");

                entity.Property(e => e.Postbr)
                    .HasMaxLength(5)
                    .HasColumnName("postbr");
            });

            modelBuilder.Entity<Osoba>(entity =>
            {
                entity.HasKey(e => e.Idosoba)
                    .HasName("osoba_pkey");

                entity.ToTable("osoba");

                entity.Property(e => e.Idosoba).HasColumnName("idosoba");

                entity.Property(e => e.Adresa)
                    .HasMaxLength(50)
                    .HasColumnName("adresa");

                entity.Property(e => e.Email)
                    .HasMaxLength(50)
                    .HasColumnName("email");

                entity.Property(e => e.Idmjesto).HasColumnName("idmjesto");

                entity.Property(e => e.Ime)
                    .HasMaxLength(50)
                    .HasColumnName("ime");

                entity.Property(e => e.Prezime)
                    .HasMaxLength(50)
                    .HasColumnName("prezime");

                entity.Property(e => e.Telefon)
                    .HasMaxLength(20)
                    .HasColumnName("telefon");

                entity.HasOne(d => d.IdmjestoNavigation)
                    .WithMany(p => p.Osobas)
                    .HasForeignKey(d => d.Idmjesto)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("osoba_idmjesto_fkey");
            });

            modelBuilder.Entity<Proizvodac>(entity =>
            {
                entity.HasKey(e => e.Idproizvodac)
                    .HasName("proizvodac_pkey");

                entity.ToTable("proizvodac");

                entity.Property(e => e.Idproizvodac).HasColumnName("idproizvodac");

                entity.Property(e => e.Nazproizvodac)
                    .HasMaxLength(50)
                    .HasColumnName("nazproizvodac");
            });

            modelBuilder.Entity<Rad>(entity =>
            {
                entity.HasKey(e => e.Idrad)
                    .HasName("rad_pkey");

                entity.ToTable("rad");

                entity.Property(e => e.Idrad).HasColumnName("idrad");

                entity.Property(e => e.Cijena).HasColumnName("cijena");

                entity.Property(e => e.Nazrad)
                    .HasMaxLength(50)
                    .HasColumnName("nazrad");
            });

            modelBuilder.Entity<Radnik>(entity =>
            {
                entity.HasKey(e => e.Idradnik)
                    .HasName("radnik_pkey");

                entity.ToTable("radnik");

                entity.Property(e => e.Idradnik).HasColumnName("idradnik");

                entity.Property(e => e.Idosoba).HasColumnName("idosoba");

                entity.Property(e => e.Idservis).HasColumnName("idservis");

                entity.Property(e => e.Lozinka)
                    .HasMaxLength(50)
                    .HasColumnName("lozinka");

                entity.Property(e => e.Voditelj).HasColumnName("voditelj");

                entity.Property(e => e.Zaposlendo).HasColumnName("zaposlendo");

                entity.Property(e => e.Zaposlenod).HasColumnName("zaposlenod");

                entity.HasOne(d => d.IdosobaNavigation)
                    .WithMany(p => p.Radniks)
                    .HasForeignKey(d => d.Idosoba)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("radnik_idosoba_fkey");

                entity.HasOne(d => d.IdservisNavigation)
                    .WithMany(p => p.Radniks)
                    .HasForeignKey(d => d.Idservis)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("radnik_idservis_fkey");
            });

            modelBuilder.Entity<Radninalog>(entity =>
            {
                entity.HasKey(e => e.Idradninalog)
                    .HasName("radninalog_pkey");

                entity.ToTable("radninalog");

                entity.Property(e => e.Idradninalog).HasColumnName("idradninalog");

                entity.Property(e => e.Idbicikl).HasColumnName("idbicikl");

                entity.Property(e => e.Idosoba).HasColumnName("idosoba");

                entity.Property(e => e.Idservis).HasColumnName("idservis");

                entity.Property(e => e.Idstatus).HasColumnName("idstatus");

                entity.Property(e => e.Izradio).HasColumnName("izradio");

                entity.Property(e => e.Obavio).HasColumnName("obavio");

                entity.Property(e => e.Vrizrada)
                    .HasColumnType("timestamp without time zone")
                    .HasColumnName("vrizrada");

                entity.Property(e => e.Vrpocetak)
                    .HasColumnType("timestamp without time zone")
                    .HasColumnName("vrpocetak");

                entity.Property(e => e.Vrzavrsetak)
                    .HasColumnType("timestamp without time zone")
                    .HasColumnName("vrzavrsetak");

                entity.HasOne(d => d.IdbiciklNavigation)
                    .WithMany(p => p.Radninalogs)
                    .HasForeignKey(d => d.Idbicikl)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("radninalog_idbicikl_fkey");

                entity.HasOne(d => d.IdosobaNavigation)
                    .WithMany(p => p.Radninalogs)
                    .HasForeignKey(d => d.Idosoba)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("radninalog_idosoba_fkey");

                entity.HasOne(d => d.IdservisNavigation)
                    .WithMany(p => p.Radninalogs)
                    .HasForeignKey(d => d.Idservis)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("radninalog_idservis_fkey");

                entity.HasOne(d => d.IdstatusNavigation)
                    .WithMany(p => p.Radninalogs)
                    .HasForeignKey(d => d.Idstatus)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("radninalog_idstatus_fkey");

                entity.HasOne(d => d.IzradioNavigation)
                    .WithMany(p => p.RadninalogIzradioNavigations)
                    .HasForeignKey(d => d.Izradio)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("radninalog_izradio_fkey");

                entity.HasOne(d => d.ObavioNavigation)
                    .WithMany(p => p.RadninalogObavioNavigations)
                    .HasForeignKey(d => d.Obavio)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("radninalog_obavio_fkey");
            });

            modelBuilder.Entity<Servi>(entity =>
            {
                entity.HasKey(e => e.Idservis)
                    .HasName("servis_pkey");

                entity.ToTable("servis");

                entity.Property(e => e.Idservis).HasColumnName("idservis");

                entity.Property(e => e.Adresaservis)
                    .HasMaxLength(50)
                    .HasColumnName("adresaservis");

                entity.Property(e => e.Idmjesto).HasColumnName("idmjesto");

                entity.Property(e => e.Nazservis)
                    .HasMaxLength(50)
                    .HasColumnName("nazservis");

                entity.Property(e => e.Vlasnikservis).HasColumnName("vlasnikservis");

                entity.HasOne(d => d.IdmjestoNavigation)
                    .WithMany(p => p.Servis)
                    .HasForeignKey(d => d.Idmjesto)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("servis_idmjesto_fkey");

                entity.HasOne(d => d.VlasnikservisNavigation)
                    .WithMany(p => p.Servis)
                    .HasForeignKey(d => d.Vlasnikservis)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("servis_vlasnikservis_fkey");
            });

            modelBuilder.Entity<Skladiste>(entity =>
            {
                entity.HasKey(e => e.Idskladiste)
                    .HasName("skladiste_pkey");

                entity.ToTable("skladiste");

                entity.Property(e => e.Idskladiste).HasColumnName("idskladiste");

                entity.Property(e => e.Nazskladiste)
                    .HasMaxLength(50)
                    .HasColumnName("nazskladiste");
            });

            modelBuilder.Entity<Statusposla>(entity =>
            {
                entity.HasKey(e => e.Idstatus)
                    .HasName("statusposla_pkey");

                entity.ToTable("statusposla");

                entity.Property(e => e.Idstatus).HasColumnName("idstatus");

                entity.Property(e => e.Nazstatus)
                    .HasMaxLength(50)
                    .HasColumnName("nazstatus");
            });

            modelBuilder.Entity<Stavkanalog>(entity =>
            {
                entity.HasKey(e => e.Idstavka)
                    .HasName("stavkanalog_pkey");

                entity.ToTable("stavkanalog");

                entity.Property(e => e.Idstavka).HasColumnName("idstavka");

                entity.Property(e => e.Idartiklservis).HasColumnName("idartiklservis");

                entity.Property(e => e.Idrad).HasColumnName("idrad");

                entity.Property(e => e.Idradninalog).HasColumnName("idradninalog");

                entity.Property(e => e.Idstatus).HasColumnName("idstatus");

                entity.Property(e => e.Kolicinaartikl).HasColumnName("kolicinaartikl");

                entity.Property(e => e.Kolicinarad).HasColumnName("kolicinarad");

                entity.HasOne(d => d.IdartiklservisNavigation)
                    .WithMany(p => p.Stavkanalogs)
                    .HasForeignKey(d => d.Idartiklservis)
                    .HasConstraintName("stavkanalog_idartiklservis_fkey");

                entity.HasOne(d => d.IdradNavigation)
                    .WithMany(p => p.Stavkanalogs)
                    .HasForeignKey(d => d.Idrad)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("stavkanalog_idrad_fkey");

                entity.HasOne(d => d.IdradninalogNavigation)
                    .WithMany(p => p.Stavkanalogs)
                    .HasForeignKey(d => d.Idradninalog)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("stavkanalog_idradninalog_fkey");

                entity.HasOne(d => d.IdstatusNavigation)
                    .WithMany(p => p.Stavkanalogs)
                    .HasForeignKey(d => d.Idstatus)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("stavkanalog_idstatus_fkey");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
