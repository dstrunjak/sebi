﻿using System;
using System.Collections.Generic;

namespace Infrastructure.EFModels
{
    public partial class Osoba
    {
        public Osoba()
        {
            Bicikls = new HashSet<Bicikl>();
            Radniks = new HashSet<Radnik>();
            Radninalogs = new HashSet<Radninalog>();
            Servis = new HashSet<Servi>();
        }

        public int Idosoba { get; set; }
        public string Ime { get; set; } = null!;
        public string Prezime { get; set; } = null!;
        public string Email { get; set; } = null!;
        public string Telefon { get; set; } = null!;
        public string Adresa { get; set; } = null!;
        public int Idmjesto { get; set; }

        public virtual Mjesto IdmjestoNavigation { get; set; } = null!;
        public virtual ICollection<Bicikl> Bicikls { get; set; }
        public virtual ICollection<Radnik> Radniks { get; set; }
        public virtual ICollection<Radninalog> Radninalogs { get; set; }
        public virtual ICollection<Servi> Servis { get; set; }
    }
}
