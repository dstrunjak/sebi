﻿using System;
using System.Collections.Generic;

namespace Infrastructure.EFModels
{
    public partial class Skladiste
    {
        public Skladiste()
        {
            Artiklservis = new HashSet<Artiklservi>();
        }

        public int Idskladiste { get; set; }
        public string Nazskladiste { get; set; } = null!;

        public virtual ICollection<Artiklservi> Artiklservis { get; set; }
    }
}
