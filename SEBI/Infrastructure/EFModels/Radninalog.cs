﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;


namespace Infrastructure.EFModels
{
    public partial class Radninalog
    {
        public Radninalog()
        {
            Stavkanalogs = new HashSet<Stavkanalog>();
        }

        [Display(Name = "Šifra")]
        public int Idradninalog { get; set; }

        [Display(Name = "Vrijeme izrade", Prompt="Vrijeme izrade")]
        public DateTime Vrizrada { get; set; }

        [Display(Name = "Vrijeme početka", Prompt="Vrijeme početka")]
        public DateTime? Vrpocetak { get; set; }

        [Display(Name = "Vrijeme završetka", Prompt="Vrijeme završetka")]
        public DateTime? Vrzavrsetak { get; set; }

        [Display(Name = "Servis", Prompt="Servis")]
        public int Idservis { get; set; }

        [Display(Name = "Korisnik", Prompt="Korisnik")]
        public int Idosoba { get; set; }

        [Display(Name = "Izradio", Prompt="Izradio")]
        public int Izradio { get; set; }

        [Display(Name = "Bicikl", Prompt="Bicikl")]
        public int Idbicikl { get; set; }

        [Display(Name = "Status", Prompt="Status")]
        public int Idstatus { get; set; }

        [Display(Name = "Obavio", Prompt="Obavio")]
        public int? Obavio { get; set; }

        public virtual Bicikl IdbiciklNavigation { get; set; } = null!;
        public virtual Osoba IdosobaNavigation { get; set; } = null!;
        public virtual Servi IdservisNavigation { get; set; } = null!;
        public virtual Statusposla IdstatusNavigation { get; set; } = null!;
        public virtual Radnik IzradioNavigation { get; set; } = null!;
        public virtual Radnik ObavioNavigation { get; set; } = null!;
        public virtual ICollection<Stavkanalog> Stavkanalogs { get; set; }
    }
}
