﻿using System;
using System.Collections.Generic;

namespace Infrastructure.EFModels
{
    public partial class Stavkanalog
    {
        public int Idstavka { get; set; }
        public int? Kolicinaartikl { get; set; }
        public int Kolicinarad { get; set; }
        public int Idradninalog { get; set; }
        public int Idrad { get; set; }
        public int Idstatus { get; set; }
        public int? Idartiklservis { get; set; }

        public virtual Artiklservi? IdartiklservisNavigation { get; set; }
        public virtual Rad IdradNavigation { get; set; } = null!;
        public virtual Radninalog IdradninalogNavigation { get; set; } = null!;
        public virtual Statusposla IdstatusNavigation { get; set; } = null!;
    }
}
