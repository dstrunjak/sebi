﻿using System;
using System.Collections.Generic;

namespace Infrastructure.EFModels
{
    public partial class Artiklservi
    {
        public Artiklservi()
        {
            Stavkanalogs = new HashSet<Stavkanalog>();
        }

        public int Idartiklservis { get; set; }
        public int Kolicina { get; set; }
        public double Cijenakom { get; set; }
        public int Idservis { get; set; }
        public int Idskladiste { get; set; }
        public int Idartikl { get; set; }

        public virtual Artikl IdartiklNavigation { get; set; } = null!;
        public virtual Servi IdservisNavigation { get; set; } = null!;
        public virtual Skladiste IdskladisteNavigation { get; set; } = null!;
        public virtual ICollection<Stavkanalog> Stavkanalogs { get; set; }
    }
}
