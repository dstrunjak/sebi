﻿using System;
using System.Collections.Generic;

namespace Infrastructure.EFModels
{
    public partial class Statusposla
    {
        public Statusposla()
        {
            Radninalogs = new HashSet<Radninalog>();
            Stavkanalogs = new HashSet<Stavkanalog>();
        }

        public int Idstatus { get; set; }
        public string Nazstatus { get; set; } = null!;

        public virtual ICollection<Radninalog> Radninalogs { get; set; }
        public virtual ICollection<Stavkanalog> Stavkanalogs { get; set; }
    }
}
