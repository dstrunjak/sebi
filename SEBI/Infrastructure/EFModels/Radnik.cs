﻿using System;
using System.Collections.Generic;

namespace Infrastructure.EFModels
{
    public partial class Radnik
    {
        public Radnik()
        {
            RadninalogIzradioNavigations = new HashSet<Radninalog>();
            RadninalogObavioNavigations = new HashSet<Radninalog>();
        }

        public int Idradnik { get; set; }
        public int Voditelj { get; set; }
        public DateOnly Zaposlenod { get; set; }
        public DateOnly? Zaposlendo { get; set; }
        public string? Lozinka { get; set; }
        public int Idosoba { get; set; }
        public int Idservis { get; set; }

        public virtual Osoba IdosobaNavigation { get; set; } = null!;
        public virtual Servi IdservisNavigation { get; set; } = null!;
        public virtual ICollection<Radninalog> RadninalogIzradioNavigations { get; set; }
        public virtual ICollection<Radninalog> RadninalogObavioNavigations { get; set; }
    }
}
