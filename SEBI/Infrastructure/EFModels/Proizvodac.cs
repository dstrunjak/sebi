﻿using System;
using System.Collections.Generic;

namespace Infrastructure.EFModels
{
    public partial class Proizvodac
    {
        public Proizvodac()
        {
            Artikls = new HashSet<Artikl>();
            Bicikls = new HashSet<Bicikl>();
        }

        public int Idproizvodac { get; set; }
        public string Nazproizvodac { get; set; } = null!;

        public virtual ICollection<Artikl> Artikls { get; set; }
        public virtual ICollection<Bicikl> Bicikls { get; set; }
    }
}
