﻿using System;
using System.Collections.Generic;

namespace Infrastructure.EFModels
{
    public partial class Bicikl
    {
        public Bicikl()
        {
            Radninalogs = new HashSet<Radninalog>();
        }

        public int Idbicikl { get; set; }
        public string Nazbicikl { get; set; } = null!;
        public string Serbrbicikl { get; set; } = null!;
        public int Vlasnikbicikl { get; set; }
        public int Idproizvodac { get; set; }

        public virtual Proizvodac IdproizvodacNavigation { get; set; } = null!;
        public virtual Osoba VlasnikbiciklNavigation { get; set; } = null!;
        public virtual ICollection<Radninalog> Radninalogs { get; set; }
    }
}
