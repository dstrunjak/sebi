﻿using System;
using System.Collections.Generic;

namespace Infrastructure.EFModels
{
    public partial class Servi
    {
        public Servi()
        {
            Artiklservis = new HashSet<Artiklservi>();
            Radniks = new HashSet<Radnik>();
            Radninalogs = new HashSet<Radninalog>();
        }

        public int Idservis { get; set; }
        public string Nazservis { get; set; } = null!;
        public string Adresaservis { get; set; } = null!;
        public int Vlasnikservis { get; set; }
        public int Idmjesto { get; set; }

        public virtual Mjesto IdmjestoNavigation { get; set; } = null!;
        public virtual Osoba VlasnikservisNavigation { get; set; } = null!;
        public virtual ICollection<Artiklservi> Artiklservis { get; set; }
        public virtual ICollection<Radnik> Radniks { get; set; }
        public virtual ICollection<Radninalog> Radninalogs { get; set; }
    }
}
