﻿using System;
using System.Collections.Generic;

namespace Infrastructure.EFModels
{
    public partial class Artikl
    {
        public Artikl()
        {
            Artiklservis = new HashSet<Artiklservi>();
        }

        public int Idartikl { get; set; }
        public string Nazartikl { get; set; } = null!;
        public string Serbrartikl { get; set; } = null!;
        public int Idproizvodac { get; set; }

        public virtual Proizvodac IdproizvodacNavigation { get; set; } = null!;
        public virtual ICollection<Artiklservi> Artiklservis { get; set; }
    }
}
