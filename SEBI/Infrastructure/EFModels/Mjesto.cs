﻿using System;
using System.Collections.Generic;

namespace Infrastructure.EFModels
{
    public partial class Mjesto
    {
        public Mjesto()
        {
            Osobas = new HashSet<Osoba>();
            Servis = new HashSet<Servi>();
        }

        public int Idmjesto { get; set; }
        public string Postbr { get; set; } = null!;
        public string Nazmjesto { get; set; } = null!;

        public virtual ICollection<Osoba> Osobas { get; set; }
        public virtual ICollection<Servi> Servis { get; set; }
    }
}
