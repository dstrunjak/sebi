using DomainModel;
using DomainServices;
using Infrastructure.EFModels;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure
{
    public class StavkaNalogRepository : IStavkaNalogRepository
    {
        private readonly SEBIContext _context;

        public StavkaNalogRepository(SEBIContext _context)
        {
            this._context = _context;
        }

        public async Task<DomainModel.StavkaNalog> GetStavkaNalog(int IdStavkaNalog)
        {
            var stavka = await _context.Stavkanalogs
                                    .Where(s => s.Idstavka == IdStavkaNalog)
                                    .Select(s => new DomainModel.StavkaNalog
                                    {
                                        Idstavka = s.Idstavka,
                                        Idradninalog = s.Idradninalog,
                                        Idrad = s.Idrad,
                                        Kolicinarad = s.Kolicinarad,
                                        Nazrad = s.IdradNavigation.Nazrad,
                                        Idartiklservis = s.Idartiklservis,
                                        Kolicinaartikl = s.Kolicinaartikl,
                                        NazArtikl = s.IdartiklservisNavigation.IdartiklNavigation.Nazartikl,
                                        Idstatus = s.Idstatus,
                                        Nazstatus = s.IdstatusNavigation.Nazstatus
                                    })
                                    .FirstOrDefaultAsync();
        
            return stavka;
        }

        public async Task<int> CreateStavkaNalog(int IdRadniNalog, int IdStavkaNalog, int IdRad, int KolicinaRad,
                                    int? IdArtiklServis, int? KolicinaArtikl, int IdStatus)
        {
            if (IdArtiklServis != null) {
                var artikl = await _context.Artiklservis.FindAsync(IdArtiklServis);
                if (artikl != null) {
                    var newQ = artikl.Kolicina - KolicinaArtikl;

                    if (newQ < 0) {
                        return -1;
                    }
                    else {
                        artikl.Kolicina = newQ.HasValue ? newQ.Value : artikl.Kolicina;
                    }
                }    
            }

            var entity = new EFModels.Stavkanalog
            {   
                Idradninalog = IdRadniNalog,
                Idstavka = IdStavkaNalog,
                Idrad = IdRad,
                Kolicinarad = KolicinaRad,
                Idartiklservis = IdArtiklServis,
                Kolicinaartikl = KolicinaArtikl,
                Idstatus = IdStatus
            };
            _context.Add(entity);
            await _context.SaveChangesAsync();

            return entity.Idstavka;
        }

        public async Task<int> UpdateStavkaNalog(int IdStavkaNalog, int IdRad, int KolicinaRad,
                                    int? IdArtiklServis, int? KolicinaArtikl, int IdStatus)
        {   
            
            var entity = await _context.Stavkanalogs.FindAsync(IdStavkaNalog);

            if (entity != null)
            {   

                if (IdArtiklServis != null) {
                    var artikl = await _context.Artiklservis.FindAsync(IdArtiklServis);
                    if (artikl != null) {
                        if ( entity.Kolicinaartikl > KolicinaArtikl ) {
                            var diff = entity.Kolicinaartikl - KolicinaArtikl;
                            artikl.Kolicina += diff.Value;
                        } else {
                            var newQ = artikl.Kolicina + entity.Kolicinaartikl - KolicinaArtikl;

                            if (newQ < 0) {
                                return -1;
                            }
                            else {
                                artikl.Kolicina = newQ.HasValue ? newQ.Value : artikl.Kolicina;
                            }
                        }
                        
                    }    
                }

                entity.Idrad = IdRad;
                entity.Kolicinarad = KolicinaRad;
                entity.Idartiklservis = IdArtiklServis;
                entity.Kolicinaartikl = KolicinaArtikl;
                entity.Idstatus = IdStatus;
                await _context.SaveChangesAsync();
            }

            return IdStavkaNalog;
        }

        public async Task DeleteStavkaNalog(int IdStavkaNalog)
        {
            var stavka = await _context.Stavkanalogs.FindAsync(IdStavkaNalog);
            if (stavka != null)
            {   
                if (stavka.Idartiklservis != null && stavka.Kolicinaartikl != null) {
                    var artikl = await _context.Artiklservis.FindAsync(stavka.Idartiklservis);
                    if (artikl != null) {
                        artikl.Kolicina += stavka.Kolicinaartikl.Value;
                    } 
                          
                }
                _context.Stavkanalogs.Remove(stavka);
            }
            
            await _context.SaveChangesAsync();
        }
    }
}