using DomainModel;
using DomainServices;
using Infrastructure.EFModels;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure
{
    public class RadRepository : IRadRepository
    {
        private readonly SEBIContext _context;

        public RadRepository(SEBIContext _context)
        {
            this._context = _context;
        }

        public async Task<DomainModel.Rad> GetRad(int IdRad)
        {
            var rad = await _context.Rads
                                    .Where(r => r.Idrad == IdRad)
                                    .Select(rad => new DomainModel.Rad
                                    {
                                        Idrad = rad.Idrad,
                                        Nazrad = rad.Nazrad,
                                        Cijena = rad.Cijena
                                    })
                                    .FirstOrDefaultAsync();
        
            return rad;
        }

        public async Task<IList<DomainModel.Rad>> GetRadovi()
        {
            var radovi = await _context.Rads
                                    .Select(r => new DomainModel.Rad
                                    {
                                        Idrad = r.Idrad,
                                        Nazrad = r.Nazrad,
                                        Cijena = r.Cijena
                                    })
                                    .ToListAsync();
            return radovi;
        }

        public async Task<int> CreateRad(int IdRad, string NazRad, double Cijena)
        {
            var entitity = new EFModels.Rad
            {
                Idrad = IdRad,
                Nazrad = NazRad,
                Cijena = Cijena
            };
            _context.Add(entitity);
            await _context.SaveChangesAsync();

            return entitity.Idrad;
        }

        public async Task<int> UpdateRad(int IdRad, string NazRad, double Cijena)
        {
            var entity = await _context.Rads.FindAsync(IdRad);
            if (entity != null)
            {
                entity.Nazrad = NazRad;
                entity.Cijena = Cijena;
                await _context.SaveChangesAsync();
            }

            return IdRad;
        }

        public async Task DeleteRad(int IdRad)
        {
            var rad = await _context.Rads.FindAsync(IdRad);
            if (rad != null)
            {
                _context.Rads.Remove(rad);
            }
            
            await _context.SaveChangesAsync();
        }
    }
}