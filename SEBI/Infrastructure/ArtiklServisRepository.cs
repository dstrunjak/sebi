using DomainModel;
using DomainServices;
using Infrastructure.EFModels;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure
{
    public class ArtiklServisRepository : IArtiklServisRepository
    {
        private readonly SEBIContext _context;

        public ArtiklServisRepository(SEBIContext _context)
        {
            this._context = _context;
        }


        public async Task<IList<DomainModel.ArtiklServis>> GetArtikliServis(int IdServis)
        {
            var artikli = await _context.Artiklservis
                                    .Where(a => a.Idservis == IdServis)
                                    .Select(a => new DomainModel.ArtiklServis
                                    {
                                        Idartiklservis = a.Idartiklservis,
                                        Nazartikl = a.IdartiklNavigation.Nazartikl,
                                        Kolicina = a.Kolicina,
                                    })
                                    .ToListAsync();
            return artikli;
        }

     
    }
}