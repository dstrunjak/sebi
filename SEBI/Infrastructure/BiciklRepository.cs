using DomainModel;
using DomainServices;
using Infrastructure.EFModels;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure
{
    public class BiciklRepository : IBiciklRepository
    {
        private readonly SEBIContext _context;

        public BiciklRepository(SEBIContext _context)
        {
            this._context = _context;
        }


        public async Task<IList<DomainModel.Bicikl>> GetBicikli()
        {
            var bicikli = await _context.Bicikls
                                    .Select(b => new DomainModel.Bicikl
                                    {
                                        Idbicikl = b.Idbicikl,
                                        Nazbicikl = b.Nazbicikl,
                                        Serbrbicikl = b.Serbrbicikl,
                                        Idproizvodac = b.Idproizvodac,
                                        PunoImeVlasnik = $"{b.VlasnikbiciklNavigation.Ime} {b.VlasnikbiciklNavigation.Prezime}" 
                                    })
                                    .ToListAsync();
            return bicikli;
        }

     
    }
}