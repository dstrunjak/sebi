using DomainModel;
using DomainServices;
using Infrastructure.EFModels;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure
{
    public class ServisRepository : IServisRepository
    {
        private readonly SEBIContext _context;

        public ServisRepository(SEBIContext _context)
        {
            this._context = _context;
        }


        public async Task<IList<DomainModel.Servis>> GetServisi()
        {
            var servisi = await _context.Servis
                                    .Select(s => new DomainModel.Servis
                                    {
                                        Idservis = s.Idservis,
                                        Nazservis = s.Nazservis,
                                        Adresaservis = s.Adresaservis,
                                        Vlasnikservis = s.Vlasnikservis,
                                        Idmjesto = s.Idmjesto
                                    })
                                    .ToListAsync();
            return servisi;
        }

     
    }
}