using DomainModel;
using DomainServices;
using Infrastructure.EFModels;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure
{
    public class RadnikRepository : IRadnikRepository
    {
        private readonly SEBIContext _context;

        public RadnikRepository(SEBIContext _context)
        {
            this._context = _context;
        }


        public async Task<IList<DomainModel.Radnik>> GetRadnici()
        {
            var radnici = await _context.Radniks
                                    .Select(r => new DomainModel.Radnik
                                    {
                                        Idradnik = r.Idradnik,
                                        Ime = r.IdosobaNavigation.Ime,
                                        Prezime = r.IdosobaNavigation.Prezime
                                    })
                                    .ToListAsync();
            return radnici;
        }

     
    }
}