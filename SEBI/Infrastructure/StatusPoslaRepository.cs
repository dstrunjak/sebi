using DomainModel;
using DomainServices;
using Infrastructure.EFModels;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure
{
    public class StatusPoslaRepository : IStatusPoslaRepository
    {
        private readonly SEBIContext _context;

        public StatusPoslaRepository(SEBIContext _context)
        {
            this._context = _context;
        }


        public async Task<IList<DomainModel.Statusposla>> GetStatusiPosla()
        {
            var statusiPosla = await _context.Statusposlas
                                    .Select(s => new DomainModel.Statusposla
                                    {
                                        Idstatus = s.Idstatus,
                                        Nazstatus = s.Nazstatus
                                    })
                                    .ToListAsync();
            return statusiPosla;
        }

     
    }
}