using DomainModel;
using DomainServices;
using Infrastructure.EFModels;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure
{
    public class RadniNalogRepository : IRadniNalogRepository
    {
        private readonly SEBIContext _context;

        public RadniNalogRepository(SEBIContext _context)
        {
            this._context = _context;
        }

        public async Task<DomainModel.RadniNalog> GetRadniNalog(int IdRadniNalog)
        {
            var radniNalog = await _context.Radninalogs
                                .Where(r => r.Idradninalog == IdRadniNalog)
                                .Select(r => new DomainModel.RadniNalog
                                {
                                    Idradninalog = r.Idradninalog,
                                    Vrizrada = r.Vrizrada,
                                    Vrpocetak = r.Vrpocetak,
                                    Vrzavrsetak = r.Vrzavrsetak,
                                    Idservis = r.Idservis,
                                    NazServis = r.IdservisNavigation.Nazservis,
                                    Idosoba = r.Idosoba,
                                    PunoImeKorisnik = $"{r.IdosobaNavigation.Ime} {r.IdosobaNavigation.Prezime}",
                                    Izradio = r.Izradio,
                                    PunoImeIzradio = $"{r.IzradioNavigation.IdosobaNavigation.Ime} {r.IzradioNavigation.IdosobaNavigation.Prezime}",
                                    Idbicikl = r.Idbicikl,
                                    Nazbicikl = r.IdbiciklNavigation.Nazbicikl,
                                    Idstatus = r.Idstatus,
                                    Nazstatus = r.IdstatusNavigation.Nazstatus,
                                    Obavio = r.Obavio,
                                    PunoImeObavio = $"{r.ObavioNavigation.IdosobaNavigation.Ime} {r.ObavioNavigation.IdosobaNavigation.Prezime}"
                                })
                                .FirstOrDefaultAsync();
            
            if(radniNalog == null) {
                return radniNalog;
            } else {
                var stavkeNaloga = _context.Stavkanalogs
                                .Where(s => s.Idradninalog == radniNalog.Idradninalog)
                                .Select(s => new DomainModel.StavkaNalog
                                {
                                    Idstavka = s.Idstavka,
                                    Kolicinaartikl = s.Kolicinaartikl,
                                    Kolicinarad = s.Kolicinarad,
                                    Idrad = s.Idrad,
                                    Nazrad = s.IdradNavigation.Nazrad,
                                    Idstatus = s.Idstatus,
                                    Nazstatus = s.IdstatusNavigation.Nazstatus,
                                    Idartiklservis = s.Idartiklservis,
                                    NazArtikl = s.IdartiklservisNavigation == null ? "nema artikla" : s.IdartiklservisNavigation.IdartiklNavigation.Nazartikl,
                                })
                                .ToList();
                radniNalog.Stavke = stavkeNaloga;
            }
            
            
        
            return radniNalog;
        }
        public async Task<IList<DomainModel.RadniNalog>> GetRadniNalozi()
        {
            var radniNalozi = await _context.Radninalogs
                                    .Select(r => new DomainModel.RadniNalog
                                    {
                                        Idradninalog = r.Idradninalog,
                                        Vrizrada = r.Vrizrada,
                                        Vrpocetak = r.Vrpocetak,
                                        Vrzavrsetak = r.Vrzavrsetak,
                                        Idservis = r.Idservis,
                                        NazServis = r.IdservisNavigation.Nazservis,
                                        Idosoba = r.Idosoba,
                                        PunoImeKorisnik = $"{r.IdosobaNavigation.Ime} {r.IdosobaNavigation.Prezime}",
                                        Izradio = r.Izradio,
                                        PunoImeIzradio = $"{r.IzradioNavigation.IdosobaNavigation.Ime} {r.IzradioNavigation.IdosobaNavigation.Prezime}",
                                        Idbicikl = r.Idbicikl,
                                        Nazbicikl = r.IdbiciklNavigation.Nazbicikl,
                                        Idstatus = r.Idstatus,
                                        Nazstatus = r.IdstatusNavigation.Nazstatus,
                                        Obavio = r.Obavio,
                                        PunoImeObavio = $"{r.ObavioNavigation.IdosobaNavigation.Ime} {r.ObavioNavigation.IdosobaNavigation.Prezime}"
                                    })
                                    .ToListAsync();

            return radniNalozi;
        }

        public async Task<int> CreateRadniNalog(DateTime Vrizrada, int Idservis, int Idosoba, int Izradio, int Idbicikl, int Idstatus)
        {
            var bicikl = await _context.Bicikls
                                .Where(b => b.Idbicikl == Idbicikl)
                                .FirstOrDefaultAsync();
            var idVlasnik = bicikl.Vlasnikbicikl;

            var entitity = new EFModels.Radninalog
            {   
                // Idradninalog = 0,
                // Vrpocetak = null,
                // Vrzavrsetak = null,
                // Obavio = null,
                Vrizrada = Vrizrada,
                Idservis = Idservis,
                Idosoba = idVlasnik,
                Izradio = Izradio,
                Idbicikl = Idbicikl,
                Idstatus = Idstatus,
            };
            _context.Add(entitity);
            await _context.SaveChangesAsync();

            return entitity.Idradninalog;
        }

        public async Task<int> UpdateRadniNalog(int Idradninalog, DateTime? Vrpocetak, DateTime? Vrzavrsetak, int Idstatus, int? Obavio)
        {

            var entity = await _context.Radninalogs.FindAsync(Idradninalog);
            if (entity != null)
            {
                entity.Vrpocetak = Vrpocetak;
                entity.Vrzavrsetak = Vrzavrsetak;
                entity.Idstatus = Idstatus;
                entity.Obavio = Obavio;
                await _context.SaveChangesAsync();
            }

           return Idradninalog;
        }
        public async Task DeleteRadniNalog(int IdRadniNalog)
        {
            var radniNalog = await _context.Radninalogs.FindAsync(IdRadniNalog);
            if (radniNalog != null)
            {
                _context.Radninalogs.Remove(radniNalog);
            }
            
            await _context.SaveChangesAsync();
        }
    }
}