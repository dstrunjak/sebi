using DomainModel;

namespace DomainServices 
{
    public interface IRadnikRepository
    {
        Task<IList<Radnik>> GetRadnici();

    }
}