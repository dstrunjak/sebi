using DomainModel;

namespace DomainServices 
{
    public interface IRadRepository
    {
        Task<Rad> GetRad(int IdRad);
        Task<IList<Rad>> GetRadovi();
        Task<int> CreateRad(int IdRad, string NazRad, double Cijena);
        Task<int> UpdateRad(int IdRad, string NazRad, double Cijena);
        Task DeleteRad(int IdRad);
    }
}