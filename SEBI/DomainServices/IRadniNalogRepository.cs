using DomainModel;

namespace DomainServices 
{
    public interface IRadniNalogRepository
    {
        Task<RadniNalog> GetRadniNalog(int IdRadniNalog);
        Task<IList<RadniNalog>> GetRadniNalozi();
        Task<int> CreateRadniNalog(DateTime Vrizrada, int Idservis, int Idosoba, int Izradio, int Idbicikl, int Idstatus);
        Task<int> UpdateRadniNalog(int Idradninalog, DateTime? Vrpocetak, DateTime? Vrzavrsetak, int Idstatus, int? Obavio);
        Task DeleteRadniNalog(int IdRadniNalog);
    }
}