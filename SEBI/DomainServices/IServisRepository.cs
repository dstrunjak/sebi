using DomainModel;

namespace DomainServices 
{
    public interface IServisRepository
    {
        Task<IList<Servis>> GetServisi();

    }
}