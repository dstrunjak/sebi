using DomainModel;

namespace DomainServices 
{
    public interface IStavkaNalogRepository
    {
        Task<StavkaNalog> GetStavkaNalog(int IdStavkaNalog);
        Task<int> CreateStavkaNalog(int IdRadniNalog, int IdStavkaNalog, int IdRad, int KolicinaRad,
                                    int? IdArtiklServis, int? KolicinaArtikl, int IdStatus);
        Task<int> UpdateStavkaNalog(int IdStavkaNalog, int IdRad, int KolicinaRad,
                                    int? IdArtiklServis, int? KolicinaArtikl, int IdStatus);
        Task DeleteStavkaNalog(int IdStavkaNalog);
    }
}