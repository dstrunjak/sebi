using DomainModel;

namespace DomainServices 
{
    public interface IOsobaRepository
    {
        Task<IList<Osoba>> GetOsobe();

    }
}