using DomainModel;

namespace DomainServices 
{
    public interface IBiciklRepository
    {
        Task<IList<Bicikl>> GetBicikli();

    }
}