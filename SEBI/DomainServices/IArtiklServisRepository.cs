using DomainModel;

namespace DomainServices 
{
    public interface IArtiklServisRepository
    {
        Task<IList<ArtiklServis>> GetArtikliServis(int IdServis);

    }
}