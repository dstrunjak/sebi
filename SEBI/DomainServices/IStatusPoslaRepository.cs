using DomainModel;

namespace DomainServices 
{
    public interface IStatusPoslaRepository
    {
        Task<IList<Statusposla>> GetStatusiPosla();

    }
}