﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;


namespace SebiMvc.Models;
public class RadninalogViewModel
{


    [Display(Name = "Šifra")]
    public int Idradninalog { get; set; }

    [Display(Name = "Vrijeme izrade", Prompt="Vrijeme izrade")]
    public DateTime Vrizrada { get; set; }

    [Display(Name = "Vrijeme početka", Prompt="Vrijeme početka")]
    public DateTime? Vrpocetak { get; set; }

    [Display(Name = "Vrijeme završetka", Prompt="Vrijeme završetka")]
    public DateTime? Vrzavrsetak { get; set; }

    [Display(Name = "Servis", Prompt="Servis")]
    public int Idservis { get; set; }

    public int NazServis { get; set; }

    [Display(Name = "Korisnik", Prompt="Korisnik")]
    public int Idosoba { get; set; }

    public string PunoImeKorisnik { get; set; } = null!;

    [Display(Name = "Izradio", Prompt="Izradio")]
    public int Izradio { get; set; }
    public string PunoImeIzradio { get; set; } = null!;

    [Display(Name = "Bicikl", Prompt="Bicikl")]
    public int Idbicikl { get; set; }

    public string Nazbicikl { get; set; } = null!;


    [Display(Name = "Status", Prompt="Status")]
    public int Idstatus { get; set; }

    public string Nazstatus { get; set; } = null!;

    [Display(Name = "Obavio", Prompt="Obavio")]
    public int? Obavio { get; set; }

    public string PunoImeObavio { get; set; } = null!;

    public List<StavkanalogViewModel> Stavke { get; set; } = new List<StavkanalogViewModel>();
}

