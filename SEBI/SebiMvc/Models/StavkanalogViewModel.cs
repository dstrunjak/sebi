﻿using System;
using System.Collections.Generic;

namespace SebiMvc.Models;

public class StavkanalogViewModel
{
    public int Idstavka { get; set; }
    public int? Kolicinaartikl { get; set; }
    public int Kolicinarad { get; set; }
    public int Idradninalog { get; set; }
    public int Idrad { get; set; }
    public string Nazrad { get; set; } = null!;
    public int Idstatus { get; set; }
    public string Nazstatus { get; set; } = null!;
    public int? Idartiklservis { get; set; }
    public string NazArtikl { get; set; } = null!;
    public double Cijenakom { get; set; }


}

