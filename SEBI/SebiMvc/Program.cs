using AutoMapper;
using Microsoft.EntityFrameworkCore;
// using Microsoft.EntityFrameworkCore.Design;
// using Npgsql.EntityFrameworkCore.PostgreSQL;
using DomainServices;
using Infrastructure;
using Infrastructure.EFModels;
using SebiMvc;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddControllersWithViews();

builder.Services.AddDbContext<Infrastructure.EFModels.SEBIContext>(options =>
    options.UseNpgsql(builder.Configuration.GetConnectionString("SebiMvc")));
builder.Services.AddTransient<IRadRepository, RadRepository>();
builder.Services.AddTransient<IRadniNalogRepository, RadniNalogRepository>();
builder.Services.AddTransient<IBiciklRepository, BiciklRepository>();
builder.Services.AddTransient<IRadnikRepository, RadnikRepository>();
builder.Services.AddTransient<IServisRepository, ServisRepository>();
builder.Services.AddTransient<IStatusPoslaRepository, StatusPoslaRepository>();
builder.Services.AddTransient<IStavkaNalogRepository, StavkaNalogRepository>();
builder.Services.AddTransient<IArtiklServisRepository, ArtiklServisRepository>();


var app = builder.Build();

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Home/Error");
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();
}

app.UseHttpsRedirection();
app.UseStaticFiles();

app.UseRouting();

app.UseAuthorization();

app.MapControllerRoute(
    name: "default",
    pattern: "{controller=Home}/{action=Index}/{id?}");

app.Run();
