using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Infrastructure.EFModels;
using DomainServices;
using DomainModel;
using SebiMvc.Models;

namespace SebiMvc.Controllers
{
    public class StavkaNalogController : Controller
    {
        private readonly SEBIContext _context;
        private readonly IStavkaNalogRepository stavkaNalogRepository;
        private readonly IRadRepository radRepository;
        private readonly IArtiklServisRepository artiklServisRepository;
        private readonly IStatusPoslaRepository statusPoslaRepository;


        public StavkaNalogController(SEBIContext context,
                                    IStavkaNalogRepository stavkaNalogRepository,
                                    IRadRepository radRepository,
                                    IArtiklServisRepository artiklServisRepository,
                                    IStatusPoslaRepository statusPoslaRepository
                                    )
        {
            _context = context;
            this.stavkaNalogRepository = stavkaNalogRepository;
            this.radRepository = radRepository;
            this.artiklServisRepository = artiklServisRepository;
            this.statusPoslaRepository = statusPoslaRepository;
        }


        public async Task<IActionResult> Create(int IdRadniNalog, int IdServis)
        {   
            ViewData["Idradninalog"] = IdRadniNalog;
            ViewData["Idservis"] = IdServis;
            ViewData["Idrad"] = new SelectList(await radRepository.GetRadovi(), nameof(DomainModel.Rad.Idrad), nameof(DomainModel.Rad.Nazrad));
            ViewData["Idartiklservis"] = new SelectList(await artiklServisRepository.GetArtikliServis(IdServis), nameof(DomainModel.ArtiklServis.Idartiklservis), nameof(DomainModel.ArtiklServis.Nazartikl));
            ViewData["Idstatus"] = new SelectList(await statusPoslaRepository.GetStatusiPosla(), nameof(DomainModel.Statusposla.Idstatus), nameof(DomainModel.Statusposla.Nazstatus));

            return View();
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(int id, int Idradninalog, int Idservis, int Idstavkanalog, int Idrad, int Kolicinarad,
                                    int? Idartiklservis, int? Kolicinaartikl, int Idstatus)
        {
            if (ModelState.IsValid)
            {
                var idStavka = await stavkaNalogRepository.CreateStavkaNalog(Idradninalog,
                                                 Idstavkanalog,
                                                 Idrad,
                                                 Kolicinarad,
                                                 Idartiklservis,
                                                 Kolicinaartikl,
                                                 Idstatus);
                if (idStavka == -1) {
                    ViewData["ArtiklQError"] = "Nedovoljna količina artikla na skladištu!";
                } else {
                    return RedirectToAction(actionName: nameof(Edit), controllerName: nameof(RadniNalog), routeValues:new { id = Idradninalog });
                }                          
            }

            ViewData["Idradninalog"] = Idradninalog;
            ViewData["Idservis"] = Idservis;
            ViewData["Idrad"] = new SelectList(await radRepository.GetRadovi(), nameof(DomainModel.Rad.Idrad), nameof(DomainModel.Rad.Nazrad), Idrad);
            ViewData["Idartiklservis"] = new SelectList(await artiklServisRepository.GetArtikliServis(Idservis), nameof(DomainModel.ArtiklServis.Idartiklservis), nameof(DomainModel.ArtiklServis.Nazartikl), Idartiklservis);
            ViewData["Idstatus"] = new SelectList(await statusPoslaRepository.GetStatusiPosla(), nameof(DomainModel.Statusposla.Idstatus), nameof(DomainModel.Statusposla.Nazstatus), Idstatus);

            return View();
        }

        public async Task<IActionResult> Edit(int id, int IdServis)
        {

            var stavkaNalog = await stavkaNalogRepository.GetStavkaNalog(id);
            if (stavkaNalog == null)
            {
                return NotFound();
            }

            ViewData["Idservis"] = IdServis;
            ViewData["Idrad"] = new SelectList(await radRepository.GetRadovi(), nameof(DomainModel.Rad.Idrad), nameof(DomainModel.Rad.Nazrad));
            ViewData["Idartiklservis"] = new SelectList(await artiklServisRepository.GetArtikliServis(IdServis), nameof(DomainModel.ArtiklServis.Idartiklservis), nameof(DomainModel.ArtiklServis.Nazartikl));
            ViewData["Idstatus"] = new SelectList(await statusPoslaRepository.GetStatusiPosla(), nameof(DomainModel.Statusposla.Idstatus), nameof(DomainModel.Statusposla.Nazstatus));

            return View(stavkaNalog);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int IdRadniNalog, int IdServis, int Idstavka, int Idrad, int Kolicinarad,
                                             int? Idartiklservis, int? Kolicinaartikl, int Idstatus)
        {
 
            if (ModelState.IsValid)
            {   
                try
                {
                    var idStavka = await stavkaNalogRepository.UpdateStavkaNalog(Idstavka,
                                                                Idrad,
                                                                Kolicinarad,
                                                                Idartiklservis,
                                                                Kolicinaartikl,
                                                                Idstatus);
                    if (idStavka == -1) {
                        ViewData["ArtiklQError"] = "Nedovoljna količina artikla na skladištu!";
                    } else {
                        return RedirectToAction(actionName: nameof(Edit), controllerName: nameof(RadniNalog), routeValues : new { id = IdRadniNalog });
                    }
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!StavkaNalogExists(Idstavka))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }


            }

            ViewData["Idservis"] = IdServis;
            ViewData["Idrad"] = new SelectList(await radRepository.GetRadovi(), nameof(DomainModel.Rad.Idrad), nameof(DomainModel.Rad.Nazrad), Idrad);
            ViewData["Idartiklservis"] = new SelectList(await artiklServisRepository.GetArtikliServis(IdServis), nameof(DomainModel.ArtiklServis.Idartiklservis), nameof(DomainModel.ArtiklServis.Nazartikl), Idartiklservis);
            ViewData["Idstatus"] = new SelectList(await statusPoslaRepository.GetStatusiPosla(), nameof(DomainModel.Statusposla.Idstatus), nameof(DomainModel.Statusposla.Nazstatus), Idstatus);
            StavkaNalog stavka = await stavkaNalogRepository.GetStavkaNalog(Idstavka);
            
            return View(stavka);
        }

        public async Task<IActionResult> Delete(int id)
        {

            var stavka = await stavkaNalogRepository.GetStavkaNalog(id);
            
            if (stavka == null)
            {
                return NotFound();
            }

            return View(stavka);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id, int IdRadniNalog)
        {
            if (_context.Stavkanalogs == null)
            {
                return Problem("Entity set 'SEBIContext.Stavkanalogs'  is null.");
            }

                await stavkaNalogRepository.DeleteStavkaNalog(id);
            
            
            return RedirectToAction(actionName: nameof(Edit), controllerName: nameof(RadniNalog), routeValues : new { id = IdRadniNalog });
        }

        private bool StavkaNalogExists(int id)
        {
          return (_context.Stavkanalogs?.Any(e => e.Idstavka == id)).GetValueOrDefault();
        }
    }
}
