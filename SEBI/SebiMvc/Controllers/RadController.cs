using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Infrastructure.EFModels;
using DomainServices;
using DomainModel;
using SebiMvc.Models;

namespace SebiMvc.Controllers
{
    public class RadController : Controller
    {
        private readonly SEBIContext _context;
        private readonly IRadRepository radRepository;
        // private readonly IMapper mapper;

        public RadController(SEBIContext context, IRadRepository radRepository/*, IMapper mapper*/)
        {
            _context = context;
            this.radRepository = radRepository;
            // this.mapper = mapper;
        }

        // GET: Rad
        public async Task<IActionResult> Index()
        {   
            var data = await radRepository.GetRadovi();
            // IEnumerable<RadViewModel> radovi = mapper.Map<IList<RadViewModel>>(data);
            // IEnumerable<RadViewModel> radovi = new List<RadViewModel>();
            // foreach(var rad in radovi) {
            //     radovi.Append(mapper.Map<RadViewModel>(rad));
            // }
            // return View(radovi);
            return View(data);
        }

        // GET: Rad/Details/5
        public async Task<IActionResult> Details(int id)
        {
            var rad = await radRepository.GetRad(id);
            
            if (rad == null)
            {
                return NotFound();
            }

            // RadViewModel model = mapper.Map<RadViewModel>(rad);

            return View(rad);
        }

        // GET: Rad/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Rad/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Idrad,Nazrad,Cijena")] RadViewModel rad)
        {
            if (ModelState.IsValid)
            {
                await radRepository.CreateRad(rad.Idrad, rad.Nazrad, rad.Cijena);
                return RedirectToAction(nameof(Index));
            }
            return View(rad);
        }

        // GET: Rad/Edit/5
        public async Task<IActionResult> Edit(int id)
        {

            var rad = await radRepository.GetRad(id);
            if (rad == null)
            {
                return NotFound();
            }
            // RadViewModel model = mapper.Map<RadViewModel>(rad);

            // return View(model);
            return View(rad);
        }

        // POST: Rad/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Idrad,Nazrad,Cijena")] RadViewModel rad)
        {
            if (id != rad.Idrad)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    await radRepository.UpdateRad(rad.Idrad, rad.Nazrad, rad.Cijena);
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!RadExists(rad.Idrad))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(rad);
        }

        // GET: Rad/Delete/5
        public async Task<IActionResult> Delete(int id)
        {

            var rad = await radRepository.GetRad(id);

            if (rad == null)
            {
                return NotFound();
            }

            // RadViewModel model = mapper.Map<RadViewModel>(rad);

            // return View(model);
            return View(rad);
        }

        // POST: Rad/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            if (_context.Rads == null)
            {
                return Problem("Entity set 'SEBIContext.Rads'  is null.");
            }

                await radRepository.DeleteRad(id);
            
            
            return RedirectToAction(nameof(Index));
        }

        private bool RadExists(int id)
        {
          return (_context.Rads?.Any(e => e.Idrad == id)).GetValueOrDefault();
        }
    }
}
