using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Infrastructure.EFModels;
using DomainServices;
using DomainModel;
using SebiMvc.Models;

namespace SebiMvc.Controllers
{
    public class RadniNalogController : Controller
    {
        private readonly SEBIContext _context;
        private readonly IRadniNalogRepository radniNalogRepository;
        private readonly IBiciklRepository biciklRepository;
        private readonly IServisRepository servisRepository;
        private readonly IStatusPoslaRepository statusPoslaRepository;
        private readonly IRadnikRepository radnikRepository;
        // private readonly IMapper mapper;

        public RadniNalogController(SEBIContext context,
                                     IRadniNalogRepository radniNalogRepository,
                                     IBiciklRepository biciklRepository,
                                     IServisRepository servisRepository,
                                     IStatusPoslaRepository statusPoslaRepository,
                                     IRadnikRepository radnikRepository/*,
                                     IMapper mapper*/)
        {
            _context = context;
            this.radniNalogRepository = radniNalogRepository;
            this.biciklRepository = biciklRepository;
            this.servisRepository = servisRepository;
            this.statusPoslaRepository = statusPoslaRepository;
            this.radnikRepository = radnikRepository;
            /*this.mapper = mapper*/;
        }

        // GET: RadniNalog
        public async Task<IActionResult> Index()
        {
            var data = await radniNalogRepository.GetRadniNalozi();
            // IList<RadninalogViewModel> radniNalozi = mapper.Map<IList<RadninalogViewModel>>(data);
            // ovo mozda padne zbog mapiranja StavkeNaloga
            // return View(radniNalozi);
            return View(data);
        }

        // GET: RadniNalog/Details/5
        public async Task<IActionResult> Details(int id)
        {
            var radniNalog = await radniNalogRepository.GetRadniNalog(id);
            if (radniNalog == null)
            {
                return NotFound();
            }
            // RadninalogViewModel model = mapper.Map<RadninalogViewModel>(radniNalog);

            // return View(model);
            return View(radniNalog);
        }

        // GET: RadniNalog/Create
        public async Task<IActionResult> Create()
        {   
            ViewData["Idbicikl"] = new SelectList(await biciklRepository.GetBicikli(), nameof(DomainModel.Bicikl.Idbicikl), nameof(DomainModel.Bicikl.Nazbicikl));
            ViewData["Idservis"] = new SelectList(await servisRepository.GetServisi(), nameof(DomainModel.Servis.Idservis), nameof(DomainModel.Servis.Nazservis));
            ViewData["Idstatus"] = new SelectList(await statusPoslaRepository.GetStatusiPosla(), nameof(DomainModel.Statusposla.Idstatus), nameof(DomainModel.Statusposla.Nazstatus));
            ViewData["Izradio"] = new SelectList(await radnikRepository.GetRadnici(), nameof(DomainModel.Radnik.Idradnik), nameof(DomainModel.Radnik.PunoIme));
            return View();
        }

        // POST: RadniNalog/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(DateTime Vrizrada, int Idservis, int Idosoba,int Izradio, int Idbicikl, int Idstatus)
        {
            if (ModelState.IsValid)
            {
                var id = await radniNalogRepository.CreateRadniNalog(
                    Vrizrada,
                    Idservis,
                    Idosoba,
                    Izradio,
                    Idbicikl,
                    Idstatus
                );
                return RedirectToAction(nameof(Edit), new { id = id });
            }

            ViewData["Idbicikl"] = new SelectList(await biciklRepository.GetBicikli(), nameof(DomainModel.Bicikl.Idbicikl), nameof(DomainModel.Bicikl.Nazbicikl), Idbicikl);
            ViewData["Idservis"] = new SelectList(await servisRepository.GetServisi(), nameof(DomainModel.Servis.Idservis), nameof(DomainModel.Servis.Nazservis), Idservis);
            ViewData["Idstatus"] = new SelectList(await statusPoslaRepository.GetStatusiPosla(), nameof(DomainModel.Statusposla.Idstatus), nameof(DomainModel.Statusposla.Nazstatus), Idstatus);
            ViewData["Izradio"] = new SelectList(await radnikRepository.GetRadnici(), nameof(DomainModel.Radnik.Idradnik), nameof(DomainModel.Radnik.PunoIme), Izradio);

            // RadninalogViewModel model = mapper.Map<RadninalogViewModel>(radniNalog);

            return View();
        }

        // GET: RadniNalog/Edit/5
        public async Task<IActionResult> Edit(int id)
        {

            var radniNalog = await radniNalogRepository.GetRadniNalog(id);
            if (radniNalog == null)
            {
                return NotFound();
            }
            // ViewData["Idbicikl"] = radniNalog.Nazbicikl;
            // ViewData["Idosoba"] = radniNalog.PunoImeKorisnik;
            // ViewData["Idservis"] = radniNalog.NazServis;
            ViewData["Idstatus"] = new SelectList(await statusPoslaRepository.GetStatusiPosla(),nameof(DomainModel.Statusposla.Idstatus), nameof(DomainModel.Statusposla.Nazstatus), radniNalog.Nazstatus);
            ViewData["Obavio"] = new SelectList(await radnikRepository.GetRadnici(), nameof(DomainModel.Radnik.Idradnik), nameof(DomainModel.Radnik.PunoIme), radniNalog.PunoImeObavio);
            
            // RadninalogViewModel model = mapper.Map<RadninalogViewModel>(radniNalog);

            // return View(model);
            return View(radniNalog);
        }

        // POST: RadniNalog/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int Idradninalog, DateTime Vrpocetak, DateTime? Vrzavrsetak, int Idstatus, int? Obavio)
        {

            if (ModelState.IsValid)
            {
                try
                {
                    await radniNalogRepository.UpdateRadniNalog(Idradninalog,
                            Vrpocetak,
                            Vrzavrsetak,
                            Idstatus,
                            Obavio);
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!RadninalogExists(Idradninalog))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
                        
           
            RadniNalog radniNalog = await radniNalogRepository.GetRadniNalog(Idradninalog);
            ViewData["Idstatus"] = new SelectList(await statusPoslaRepository.GetStatusiPosla(),nameof(DomainModel.Statusposla.Idstatus), nameof(DomainModel.Statusposla.Nazstatus), radniNalog.Idstatus);
            ViewData["Obavio"] = new SelectList(await radnikRepository.GetRadnici(), nameof(DomainModel.Radnik.Idradnik), nameof(DomainModel.Radnik.PunoIme), radniNalog.Obavio);
        
            return View(radniNalog);
        }

        // GET: RadniNalog/Delete/5
        public async Task<IActionResult> Delete(int id)
        {

            var radniNalog = await radniNalogRepository.GetRadniNalog(id);
            if (radniNalog == null)
            {
                return NotFound();
            }

            // RadninalogViewModel model = mapper.Map<RadninalogViewModel>(radniNalog);

            return View(radniNalog);
        }

        // POST: RadniNalog/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            await radniNalogRepository.DeleteRadniNalog(id);
            return RedirectToAction(nameof(Index));
        }

        private bool RadninalogExists(int id)
        {
          return (_context.Radninalogs?.Any(e => e.Idradninalog == id)).GetValueOrDefault();
        }
    }
}